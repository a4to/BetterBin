#!/usr/bin/env bash

blue(){ echo -e "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
msg(){ echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"; }
err(){ echo "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"; }

btl='bluetoothctl'
sel1=$(mktemp -uq)
sel2=$(mktemp -uq)
devices=$(mktemp -uq)

trap 'rm -f $sel1 $devices $sel2' EXIT 

cat <<-EOF >$sel1
Activate Bluetooth
Connect to Device
Scan for Devices
Deactivate Bluetooth
EOF

# Devices:

echo ""$($btl  devices|cut -d' ' -f3)"-$($btl  devices|cut -d' ' -f2)" >>$devices
echo "$($btl  devices|cut -d' ' -f3)" >>$sel2 


# Main :

connectfunc(){
  $btl connect $1 && exit 0 || 
  $btl trust $1 && 
  $btl pair $1 && 
  $btl connect $1 && exit 0
}

activatefunc(){
  $btl power on  >/dev/null 2>&1 && msg "Changing power on succeeded ✔" || err "Changing power on failed ✖"
  $btl discoverable on >/dev/null 2>&1 && msg "Changing discoverable on succeeded ✔" || err "Changing discoverable on failed ✖"
  $btl pairable on >/dev/null 2>&1 && msg "Changing pairable on succeeded ✔" || err "Changing pairable on failed ✖"
  $btl agent on >/dev/null 2>&1 && msg "Changing agent on succeeded ✔"
  $btl default-agent on >/dev/null 2>&1 && msg "Changing default-agent on succeeded ✔"
}

sel1func(){
clear ; echo -e "\n" ;
pair="$(cat $sel1|fzf \
  --margin=20%,40%,40%,40% \
  --color bg:"#222222",preview-bg:"#333333" \
  --prompt "              Action :" \
  --layout reverse \
  --border rounded \
  --keep-right \
  --header " " \
  --no-info \
  --bold)"     

clear && echo -e "\n" 
case $pair in
  *Device) sel2func ;;
  *Activate*) activatefunc ;;
  Scan*) clear && msg "\n  Scanning for devices...\n" && $btl scan on ;;
  *Deactivate*) $btl power off && msg "Changing power off succeeded ✔" || err "Changing power off failed ✖" ;;
  *) echo "Not found" ;; 
esac ; }


sel2func(){
clear ; echo -e "\n"
dev="$(cat $sel2|fzf \
  --height=20 \
  --margin=18%,40%,40%,40% \
  --color bg:"#222222",preview-bg:"#333333" \
  --no-extended \
  --layout reverse \
  --header " " \
  --prompt "          Select Device :" \
  --no-info \
  --disabled \
  --border \
  --bold \
  --keep-right)"
 
clear && echo -e "\n" ;
[ $(cat $devices|grep $dev) ] && 
  connectfunc $(cat $devices|grep $dev|cut -d'-' -f2) &&
  msg "Connected to $x ✔" && exit 0 || 
  err "Failed to Connect to $x ✖" && 
  exit 1 ; }

sel1func

